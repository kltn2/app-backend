const db = require("../db");
const { protectedRoute, responses } = require("./context");
const { SUCCESS, FAILED, NOTFOUND } = responses();

// const dbCoupon = db.Coupon;
// const nameCoupon = "Coupon";

const getByOrder = async (ctx) => {};
const getByCustomer = async (ctx) => {};
const getByFanPage = async (ctx) => {};
const getByProduct = async (ctx) => {};

module.exports = {
	attach(router) {
		router.get("/reports/orders", (ctx) =>
			protectedRoute("user", getByOrder, ctx)
		);
		router.get("/reports/customer", (ctx) =>
			protectedRoute("user", getByCustomer, ctx)
		);
		router.get("/reports/page", (ctx) =>
			protectedRoute("user", getByFanPage, ctx)
		);
		router.get("/reports/product", (ctx) =>
			protectedRoute("user", getByProduct, ctx)
		);
	},
};
