const db = require("../db");
const { protectedRoute, responses } = require("./context");
const { SUCCESS, FAILED, NOTFOUND } = responses();

const dbShipping = db.ShippingInformation;
const nameShipping = "Shipping Information";

const getAll = async (ctx) => {
	let cond = null;
	let paging = null;
	let sort = null;
	const { page, size, sortBy, sortDirection } = ctx.query;
	const { user } = ctx.state;

	if (user) {
		cond = { userId: user.id };
		// Sort
		if (sortBy && sortDirection) {
			sort = { sortBy, sortDirection };
		}

		// Pagination
		if (page && size) {
			paging = { page, size };
		}

		const result = await dbShipping.findAll(cond, sort, paging);

		if (result.error) {
			return FAILED(ctx, result.error);
		}

		return SUCCESS(ctx, result, "Get", nameShipping);
	}
};

const getById = async (ctx) => {
	const { user } = ctx.state;
	const { id } = ctx.params;

	if (user && id) {
		cond = { id, userId: user.id };

		let result = null;
		try {
			result = await dbShipping.findOne(cond);
			// Not found
			if (result.error) {
				return NOTFOUND(ctx, nameShipping);
			}
		} catch (error) {
			return FAILED(ctx, error);
		}

		return SUCCESS(ctx, result, "Get", nameShipping);
	}
};

const create = async (ctx) => {
	const { user } = ctx.state;
	const { body } = ctx.request;

	if (user && body) {
		const params = { userId: user.id, ...body };

		const result = await dbShipping.save(params);
		if (result.error) {
			return FAILED(ctx, result.error);
		}

		return SUCCESS(ctx, result, "Create", nameShipping);
	}
};

const update = async (ctx) => {
	const { user } = ctx.state;
	const { body } = ctx.request;

	if (user && body) {
		const { id } = body;
		const params = { userId: user.id, ...body };

		let result = null;
		try {
			result = await dbShipping.update(id, params);
			// Not found
			if (result.error) {
				return NOTFOUND(ctx, nameShipping);
			}
		} catch (error) {
			return FAILED(ctx, error);
		}

		return SUCCESS(ctx, result, "Update", nameShipping);
	}
};

const destroy = async (ctx) => {
	const { user } = ctx.state;
	const { body } = ctx.request;

	if (user && body) {
		const { ids } = body;

		let result = null;
		try {
			result = await dbShipping.destroy(ids);
			// Not found
			if (result.error) {
				return NOTFOUND(ctx, nameShipping);
			}
		} catch (error) {
			return FAILED(ctx, error);
		}

		return SUCCESS(ctx, result, "Destroyed", nameShipping);
	}
};

const hardDelete = async (ctx) => {
	const { user } = ctx.state;
	const { body } = ctx.request;

	if (user && body) {
		const { ids } = body;

		let result = null;
		try {
			result = await dbShipping.hardDelete(ids);
			// Not found
			if (result.error) {
				return NOTFOUND(ctx, nameShipping);
			}
		} catch (error) {
			return FAILED(ctx, error);
		}

		return SUCCESS(ctx, result, "Destroyed", nameShipping);
	}
};

module.exports = {
	attach(router) {
		router.get("/shippings", (ctx) => protectedRoute("user", getAll, ctx));
		router.get("/shippings/:id", (ctx) =>
			protectedRoute("user", getById, ctx)
		);
		router.post("/shippings", (ctx) => protectedRoute("user", create, ctx));
		router.put("/shippings", (ctx) => protectedRoute("user", update, ctx));
		router.delete("/shippings", (ctx) =>
			protectedRoute("user", destroy, ctx)
		);
		router.delete("/shippings/hard", (ctx) =>
			protectedRoute("admin", hardDelete, ctx)
		);
	},
};
