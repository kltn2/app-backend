const _ = require("lodash");
const db = require("../../db");

const { protectedRoute, responses } = require("../context");
const { SUCCESS, FAILED, NOTFOUND } = responses();

const dbReceiptDetail = db.GoodsReceiptNoteDetail;
const dbReceipt = db.GoodsReceiptNote;
const nameReceipt = "Receipt Import";

// Lay ra danh sach variants Nhap Kho -> SL Nhap kho
// Tim variant nay trong Xuat Kho -> SL Xuat kho -> Khong co tra ve 0
// Tim variant nay trong order -> SL Khach dat
// Tinh ra SL Ton kho
const getAll = async (ctx) => {
	let cond = null;
	let paging = null;
	let sort = null;
	const { page, size, sortBy, sortDirection } = ctx.query;
	const { user } = ctx.state;

	if (user) {
		cond = { userId: user.id };
		// Sort
		if (sortBy && sortDirection) {
			sort = { sortBy, sortDirection };
		}

		// Pagination
		if (page && size) {
			paging = { page, size };
		}
		let result = await dbReceipt.findAll(cond, sort, paging);
		if (result.error) {
			return FAILED(ctx, result.error);
		}

		return SUCCESS(ctx, result, "Get", nameReceipt);
	}
};

module.exports = {
	attach(router) {
		router.get("/warehouse/inventory", (ctx) =>
			protectedRoute("user", getAll, ctx)
		);
	},
};
